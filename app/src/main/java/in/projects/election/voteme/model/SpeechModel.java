package in.projects.election.voteme.model;

public class SpeechModel {
	String _id;
	String title;
	String description;


	public SpeechModel(String name, String descriptoin) {
		super();
		this.title = name;
		this.description = descriptoin;
	}
	
	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}



}
