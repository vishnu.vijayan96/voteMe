package in.projects.election.voteme.model;

import java.util.ArrayList;


public class ResponseConstitutionComments extends SimpleResponse{
	public Data data;

	public ResponseConstitutionComments(Data data, String status, String message) {
		super(status, message);
		this.data = data;
	}

	public class Data {
		ArrayList<IdeaModel> ideas = new ArrayList<IdeaModel>();

		public ArrayList<IdeaModel> getIdeas() {
			return ideas;
		}

		public void setIdeas(ArrayList<IdeaModel> ideas) {
			this.ideas = ideas;
		}
		
	}
}
