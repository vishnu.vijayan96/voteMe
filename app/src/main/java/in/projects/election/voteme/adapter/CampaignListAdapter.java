package in.projects.election.voteme.adapter;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import in.projects.election.voteme.R;
import in.projects.election.voteme.model.CampaignModel;

public class CampaignListAdapter extends BaseAdapter{
	Activity adapterContext;
	ArrayList<CampaignModel> campaignList = new ArrayList<CampaignModel>();
	ViewHolder viewholder;
	LayoutInflater mInflator;
	public CampaignListAdapter(Activity context, ArrayList<CampaignModel> data){
		campaignList = data;
		adapterContext = context;
		mInflator = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return campaignList.size();
	}

	@Override
	public Object getItem(int pos) {
		return campaignList.get(pos);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		try{
			if(view == null){
				view = mInflator.inflate(R.layout.listitem_campaign, null);
				viewholder = new ViewHolder();
				viewholder.txtTitle = (TextView) view.findViewById(R.id.txt_item_title);
                viewholder.txtDescpn = (TextView) view.findViewById(R.id.txt_item_descpn);
                viewholder.txtPlace = (TextView) view.findViewById(R.id.txt_item_place);
                viewholder.txtTime = (TextView) view.findViewById(R.id.txt_item_date_time);


				view.setTag(viewholder);
			}else{
				viewholder = (ViewHolder) view.getTag();
			}
			
			viewholder.txtTitle.setText(campaignList.get(position).getTitle());
            viewholder.txtDescpn.setText(campaignList.get(position).getDescription());
            viewholder.txtPlace.setText(campaignList.get(position).getPlace());
            viewholder.txtTime.setText(campaignList.get(position).getDate()+" "+campaignList.get(position).getTime());
            if(position%2 != 0)
                view.setBackgroundColor(Color.TRANSPARENT);


		}catch (Exception e){
			e.printStackTrace();
		}
		return view;
	}

	public class ViewHolder{
		TextView txtTitle;
		TextView txtDescpn;
		TextView txtPlace;
		TextView txtTime;


	}


}






	