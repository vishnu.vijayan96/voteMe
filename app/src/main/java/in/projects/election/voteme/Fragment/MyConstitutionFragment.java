package in.projects.election.voteme.Fragment;

import java.util.ArrayList;

import com.google.gson.Gson;

import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;


import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.adapter.IdeaListAdapter;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.db.DBConstants;
import in.projects.election.voteme.db.ElectionDatabase;
import in.projects.election.voteme.model.IdeaModel;
import in.projects.election.voteme.model.ResponseConstitutionComments;

public class MyConstitutionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
	private View rootView;
	private FrameLayout containerId;
	private FloatingActionButton btnShareIdea;
	private ListView listIdeas;
	private IdeaListAdapter adapter = null;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar mProgress;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppCompatActivity actionBarActivity = (AppCompatActivity) getActivity();
		ActionBar actionBar = actionBarActivity.getSupportActionBar();
		if (null != actionBar) {
			actionBar.setTitle(getResources().getString(R.string.title_constitution));
			actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
		}



    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		rootView = inflater.inflate(R.layout.fragment_constitution, container, false);
		containerId = (FrameLayout) this.getActivity().findViewById(R.id.container_body);
		bindControls();
		return rootView;
	}

	private void bindControls(){
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(this);
		btnShareIdea = (FloatingActionButton) rootView.findViewById(R.id.btn_menu_action);
		btnShareIdea.setVisibility(View.VISIBLE);
        mProgress = (ProgressBar) rootView.findViewById(R.id.progressbar);
		listIdeas = (ListView) rootView.findViewById(R.id.list_idea);
		btnShareIdea.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				LoadIdeaFragment();

			}
		});
	}

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        getDataFromServer();
    }

    private void LoadIdeaFragment() {
        Fragment fragment = new ShareIdeaFragment();
        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerId.getId(), fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }

	ArrayList<IdeaModel> IdeaList = new ArrayList<IdeaModel>();

	
	@Override
	public void onResume() {
		super.onResume();
        checkForIdeasInDB();
        if(IdeaList.size() <= 0)
          getDataFromServer();
        else{
            adapter = new IdeaListAdapter(getActivity(), IdeaList);
            listIdeas.setAdapter(adapter);
        }
	}
	
	private void getDataFromServer(){
        Utils.showProgress(getActivity(),"Loading...");
		 APIJSONHandler apijsonHandler = new APIJSONHandler();
         apijsonHandler.getMandalamDataAPI(Singletons.getInstance().getAuth_key(), new APIListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete(String response) {

                Log.e(Finals.TAG, "Json :"+response);
                try{
                    Gson gson = new Gson();
                    ResponseConstitutionComments responseComments = gson.fromJson(response, ResponseConstitutionComments.class);
                    if(responseComments != null){
                        IdeaList.clear();
                        IdeaList.addAll(responseComments.data.getIdeas());
                        adapter = new IdeaListAdapter(getActivity(), IdeaList);
                        listIdeas.setAdapter(adapter);

                        insertIntoDB(responseComments.data.getIdeas());
                    }
                   Utils.hideProgress();
                }catch(Exception e){
                    e.printStackTrace();
                    Utils.hideProgress();
                }
            }

            @Override
            public void onCompleteWithError(String error) {
                Utils.hideProgress();
            }
        });

	}

    private void insertIntoDB(ArrayList<IdeaModel> ideas){
        ElectionDatabase db = new ElectionDatabase(getActivity());
        db.clearTable(DBConstants.Tables.mandalamTable);
        db.insertMandalamData(ideas);
    }

    private void checkForIdeasInDB(){
        ArrayList<IdeaModel> ideaList = new ArrayList<IdeaModel>();
        ElectionDatabase db = new ElectionDatabase(getActivity());
        ideaList = db.getMandalamData();
        if(ideaList != null && ideaList.size() > 0)
           this.IdeaList = ideaList;

    }
}
