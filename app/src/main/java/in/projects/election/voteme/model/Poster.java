package in.projects.election.voteme.model;

import android.graphics.Bitmap;

public class Poster {
	String _id;
	String name;
	String poster_url;
	String poster_image;
	
	public Poster(String name, String poster_url) {
		super();
		this.name = name;
		this.poster_url = poster_url;
	}
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPoster_url() {
		return poster_url;
	}
	public void setPoster_url(String poster_url) {
		this.poster_url = poster_url;
	}

	public String getPoster_image() {
		return poster_image;
	}

	public void setPoster_image(String poster_image) {
		this.poster_image = poster_image;
	}
}
