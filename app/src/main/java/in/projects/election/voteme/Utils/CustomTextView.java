package in.projects.election.voteme.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by tltvmd15 on 14/3/16.
 */
public class CustomTextView extends TextView {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public CustomTextView(Context context) {
        super(context);

        applyCustomFont(context, null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);

        setTypeface(Utils.getTypeface(context),textStyle);
    }

    public static class Utils {
        private static Typeface tf;

        public static Typeface getTypeface(Context ctx) {
            if (tf == null) {
                tf = Typeface.createFromAsset(ctx.getAssets(),
                        "fonts/helvetica.otf");
            }
            return tf;
        }
    }
}