package in.projects.election.voteme.Fragment;


import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.model.ResponseSelfie;

/**
 * Created by Pratheeja on 14/3/16.
 */
public class SelfieFragment extends Fragment implements View.OnClickListener{
    private View rootView;
    private FrameLayout containerId;

    private Button btnTakePhoto;
    private Button btnDownloadPoster;
    private ImageView imgProfilePic;
    private ProgressBar mProgress;
    private String posterUrl = "";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File proFile= new File(Finals.POSTERS_PATH);
        proFile.mkdirs();
        AppCompatActivity actionBarActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        if (null != actionBar) {
            actionBar.setTitle(getResources().getString(R.string.title_selfie));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_selfie_corner, container, false);
        containerId = (FrameLayout) this.getActivity().findViewById(R.id.container_body);
        bindControls();
        return rootView;
    }

    private void bindControls(){
        btnTakePhoto = (Button) rootView.findViewById(R.id.btn_take_photo);
        btnDownloadPoster = (Button) rootView.findViewById(R.id.btn_download_poster);
        imgProfilePic = (ImageView) rootView.findViewById(R.id.profile_pic);
        mProgress = (ProgressBar) rootView.findViewById(R.id.progressbar);
        btnTakePhoto.setOnClickListener(this);
        btnDownloadPoster.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_take_photo){
            takePicture();
        }else if(v.getId() == R.id.btn_download_poster){
           if(!posterUrl.equals("")) {
               Utils.showProgress(getActivity(), "Loading...");
               new DownloadImageTask(imgProfilePic).execute(posterUrl);
           }else
               Toast.makeText(getActivity(), "Poster is not available now. Please take selfie and download again",
                       Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap url = null;
      //  if(resultCode == RESULT_OK){
            if(requestCode == 1337){


                imgProfilePic.setImageDrawable(null);
                imgProfilePic.destroyDrawingCache();
                if(data.hasExtra("data")) {
                    url = (Bitmap) data.getExtras().get("data");
                    imgProfilePic.setImageBitmap(url);
                }
                sendImageToServer(url);


            }
      //  }

    }

    public static Bitmap overlay(Bitmap bmp2, Bitmap bmp1) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        int height = (bmp1.getHeight() - bmp2.getHeight())/2;
        int width = (bmp1.getWidth() - bmp2.getWidth())/2;
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, height,width, null);
        return bmOverlay;
    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.e("test",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 20, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e("test", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.e("test", "Error accessing file: " + e.getMessage());
        }
    }



    private String saveToInternalStorage(Bitmap bitmapImage) throws IOException {

        // path to /data/data/yourapp/app_data/imageDir
        File directory = new File(Environment.getExternalStorageDirectory()
                + "/electionapp");
        // Create imageDir
        File mypath=new File(directory,"tmp.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            fos.close();
        }
        return directory.getAbsolutePath();
    }

    private Bitmap loadImageFromStorage(String path)
    {

        try {
            File f=new File(path, "tmp.jpg");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }

    }

    /** Create a File for saving an image or video */
    private  File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/electionapp");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        Log.e("test","File Path : "+mediaFile.getAbsolutePath());
        Toast.makeText(getActivity(), "File path : "+mediaFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
        postToFB(mediaFile.getAbsolutePath());
        return mediaFile;
    }

    private void postToFB(String path){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        Uri imagePath = Uri.parse(path);
        shareIntent.setType("image/jpg");
        shareIntent.putExtra(Intent.EXTRA_STREAM,imagePath);
        startActivity(Intent.createChooser(shareIntent, "Share using"));
    }




    public void takePicture(){
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, 1337);
        //requestingCameraPermission();
    }


    public  void requestingCameraPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this.getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this.getActivity(),
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this.getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        1223);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }



    private void sendImageToServer(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte imageInByte[] = stream.toByteArray();
        String encodedImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);
        Log.e(Finals.TAG,"imageInByte : "+imageInByte);
        APIJSONHandler apijsonHandler = new APIJSONHandler();
        Utils.showProgress(getActivity(),"Sending...");
        apijsonHandler.imageUploadAPI(Singletons.getInstance().getAuth_key(), encodedImage, new APIListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete(String response) {

                try {
                    Gson gson = new Gson();
                    ResponseSelfie responseSelfie = gson.fromJson(response, ResponseSelfie.class);
                    if(responseSelfie != null && responseSelfie.isSuccess()){
                        posterUrl = responseSelfie.data.getImage();
                        Utils.showInformationAlert(getActivity(),getString(R.string.image_upload_msg));
                    }else{
                        Toast.makeText(getActivity(), "No Posters available", Toast.LENGTH_LONG).show();
                    }
                    Utils.hideProgress();
                }catch(Exception e){
                    e.printStackTrace();
                    Utils.hideProgress();
                }
            }

            @Override
            public void onCompleteWithError(String error) {
                Utils.hideProgress();
            }
        });

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public void writeBitmaptoFile(Bitmap bmp) throws IOException {

            File file = new File(Finals.POSTERS_PATH, "poster.jpg");

            FileOutputStream out = new FileOutputStream(file.getAbsoluteFile());
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();




        }
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                URL url = new URL(urldisplay);
                Log.e(Finals.TAG, "Url : "+urldisplay);

                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                writeBitmaptoFile(bmp);
                mIcon11 = bmp;

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            File file = new File(Finals.POSTERS_PATH, "poster.jpg");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mediaScanIntent = new Intent(
                        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(file.getAbsoluteFile()); //out is your file you saved/deleted/moved/copied
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
            } else {
                getActivity().sendBroadcast(new Intent(
                        Intent.ACTION_MEDIA_MOUNTED,
                        Uri.parse("file://"
                                + file.getAbsoluteFile())));
            }
            Utils.hideProgress();
            Utils.showInformationAlert(getActivity(),getString(R.string.poster_info));

        }
    }


    private Bitmap storeInSDCard(String link){
        Bitmap resultImage = null;
        URL url;
        InputStream input = null;
        try {
            url = new URL(link);
            input = url.openStream();
            OutputStream output = new FileOutputStream (Finals.BANNER_IMAGE_PATH);
            try {
                byte[] buffer = new byte[2048];
                int bytesRead = 0;
                while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                    output.write(buffer, 0, bytesRead);
                }
            }
            finally {
                output.close();
                resultImage = BitmapFactory.decodeStream(input);
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        finally {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultImage;
    }
}
