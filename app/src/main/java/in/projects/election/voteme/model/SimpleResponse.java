package in.projects.election.voteme.model;

public class SimpleResponse {

	private String status;
	private String message;

	public SimpleResponse(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		if (this.status != null && "success".equalsIgnoreCase(status))
			return true;
		return false;
	}

	public String getError() {
		if (this.message != null && message.length() > 0) {
			return message;
		}

		return "No Error";
	}

}
