package in.projects.election.voteme.model;

public class Contacts {
	private String name;
	private String phone_number;
	private boolean setSelection;
	
	public Contacts(String name, String phone_number,boolean selection) {
		super();
		this.name = name;
		this.phone_number = phone_number;
		this.setSelection = selection;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public boolean isSetSelection() {
		return setSelection;
	}
	public void setSetSelection(boolean setSelection) {
		this.setSelection = setSelection;
	}
	
	

}
