package in.projects.election.voteme.db;

public class DBConstants {
	
		public static final String DATABASE_NAME = "election_db";
		public static final int DATABASE_VERSION = 1;

		public class Tables {
			public static final String profileTable      = "Profile";
			public static final String mandalamTable     = "Mandalam";
			public static final String mediaTable    	 = "Media";
			public static final String postersTable      = "Posters";
			public static final String campaignTable     = "CampaignUpdate";
            public static final String speechTable       = "Speech";
		}
		public class ProfileTables {

			public static final String colProfileId      = "ProfileId";
			public static final String colCandidateName  = "CandidateName";
			public static final String colAbout		     = "About";
			public static final String colBiography	     = "Biography";
			public static final String colAgitations	 = "Agitations";
			public static final String colProfilePath	 = "ProfilePath";
			public static final String colAppShareUrl	 = "AppShareURL";
		}
		
		public class MandalamTable{
			public static final String colMandalamId     = "MandalamId";
			public static final String colMandalamTopic  = "MandalamTopic";
			public static final String colMandalamDescpn = "MandalamDescpn";
		}

		public class SpeechTable{
			public static final String colSpeechId     = "SpeechId";
			public static final String colSpeechTopic  = "SpeechTopic";
			public static final String colSpeechDescpn = "SpeechDescpn";
		}

		public class mediaTable{
			public static final String colMediaId    	 = "MediaId";
			public static final String colMediaTitle 	 = "MediaTitle";
			public static final String colMediaPath  	 = "MediaPath";
			public static final String colMediaType  	 = "MediaType";
		}
		
		public class postersTable{
			public static final String colPosterId    	 = "posterId";
			public static final String colPosterTitle 	 = "posterTitle";
			public static final String colPosterPath  	 = "posterPath";
		}

	public class campaignTable{
		public static final String colCampaignId    	 = "CampaignId";
		public static final String colCampaignTitle 	 = "CampaignTitle";
		public static final String colCampaignDescpn  	 = "CampaignDescpn";
		public static final String colCampaignPlace      = "CampaignPlace";
		public static final String colCampaignDate       = "CampaignDate";
		public static final String colCampaignTime       = "CampaignTime";


	}

}
