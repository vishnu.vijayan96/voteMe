package in.projects.election.voteme.constants;

import android.os.Environment;

import in.projects.election.voteme.R;

public class Finals {

	public static final String API_BASE = "http://dyuthy.com/electionapp/haripad/api/";
	public static final String GET_PROFILE = API_BASE+"profile";
	public static final String JOIN_PARTY = API_BASE+"join_party";
	public static final String MY_CONSTIUTION= API_BASE+"mandalam_comments";
	public static final String API_SPEECH = API_BASE+"speeches";
	public static final String SHARE_IDEA = API_BASE+"mandalam_request";
	public static final String API_DOWNLOAD_POSTER = API_BASE+"download_poster";
	public static final String API_SONGS_VIDEOS = API_BASE+"songs_videos";
	public static final String API_CAMPAIGN_UPDATE = API_BASE+"campaigns";
	public static final String API_SELFIE = API_BASE+"selfie";
	public static final String API_REGISTER_GCM = API_BASE+"device_register/";


	public static final String TAG = "VoteMe";

	public static final String BANNER_IMAGE_PATH = Environment.getExternalStorageDirectory()
			+ "/Election/Banner/banner_image.jpg";
    public static final String BANNER_IMAGE_NAME = "/banner_image.jpg";

	public static final String MEDIA_PATH = Environment.getExternalStorageDirectory()
			+ "/Election/media/";
    public static final String PROFILE_PATH = Environment.getExternalStorageDirectory()
            + "/Election/profile/";
    public static final String POSTERS_PATH = Environment.getExternalStorageDirectory()
            + "/Election/posters/";
}
