package in.projects.election.voteme.model;

public class CampaignModel {
	String _id;
	String title;
	String description;
	String place;
	String date;
	String time;

    public CampaignModel() {
    }

    public CampaignModel(String title, String description, String place, String date, String time) {
        this.title = title;
        this.description = description;
        this.place = place;
        this.date = date;
        this.time = time;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
