package in.projects.election.voteme.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import in.projects.election.voteme.Fragment.CampaignUpdatesFragment;
import in.projects.election.voteme.Fragment.FragmentDrawer;
import in.projects.election.voteme.Fragment.InviteFriendFragment;
import in.projects.election.voteme.Fragment.MyConstitutionFragment;
import in.projects.election.voteme.Fragment.PartySongsFragment;
import in.projects.election.voteme.Fragment.ProfileFragment;
import in.projects.election.voteme.Fragment.SelfieFragment;
import in.projects.election.voteme.Fragment.SpeechFragment;
import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Constants;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.pushnotification.MyGcmListenerService;
import in.projects.election.voteme.pushnotification.QuickstartPreferences;
import in.projects.election.voteme.pushnotification.RegistrationIntentService;

/**
 * Created by Pratheeja on 14/3/16.
 */
public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    //For GCM
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private String action;
    boolean doubleBackToExitPressedOnce = false;
    //public CallbackManager callbackManager;
   // private facebookListener fbListener;
    GoogleCloudMessaging gcm;
    String regid;
  //int positionFragment = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //FacebookSdk.sdkInitialize(getApplicationContext());
      //FacebookSdk.setApplicationId("222023511492711");
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setTitleTextColor(Color.WHITE);
        //callbackManager = CallbackManager.Factory.create();

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.shop_fragment_navigation_drawer);
        drawerFragment.setUp(R.id.shop_fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
         displayView(Constants.CONTANT_ZERO);

        showNotificationMsg();
/*
        if (checkPlayServices()) {
            getGCMRegId();
        }
*/
    }

    public void showNotificationMsg(){

        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            String flg=extras.getString("from");
            if(flg.equals("notification")){
                String msgStr= Singletons.getInstance().getNotification_msg();
                Utils.showInformationAlert(this,msgStr);
            }
        }

    }

    /*public CallbackManager getCallBackMAnager(){
        return callbackManager;
    }*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            case R.id.action_settings:
                // Intent intent = new Intent(this, Settings_Activity.class);
                //intent.putExtra(Constants.INTENT_SWITCH_SETTINGS, 1);
                // startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      /*  Log.e(Finals.TAG,"Position : "+positionFragment);
        if(positionFragment == 1) {
            fbListener.fbdataReceived(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }*/
    }

 /*   public void setCallBackListener(facebookListener listener){
        this.fbListener = listener;
    }*/

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 0:
                //positionFragment= 0;
                fragment = new ProfileFragment();
                title = getString(R.string.title_profile);
                break;
            case 1:
               // positionFragment= 1;
                fragment = new MyConstitutionFragment();
                title = getString(R.string.title_constitution);
                break;
            case 2:
                // positionFragment= 1;
                fragment = new SpeechFragment();
                title = getString(R.string.title_speech);
                break;
            case 3:
               // positionFragment= 2;
                fragment = new SelfieFragment();
                title = getString(R.string.title_selfie);
                break;
            case 4:
               // positionFragment= 3;
                fragment = new PartySongsFragment();
                title = getString(R.string.title_party_songs);
                break;
            case 5:
               // positionFragment= 4;
                fragment = new CampaignUpdatesFragment();
                title = getString(R.string.title_campaign_updates);
                break;
            case 6:
               // positionFragment= 5;
                fragment = new InviteFriendFragment();
                title = getString(R.string.title_invite_friends);
                break;
            case 7:
                finish();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    public void getGCMRegId() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(getResources().getString(R.string.gcm_defaultSenderId));
                    msg = "Device registered, registration ID=" + regid;
                    Log.i("GCM", msg);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    Log.i("Error", msg);
                    throw new RuntimeException(ex);

                }
                return regid;
            }

            @Override
            protected void onPostExecute(String regId) {
                Log.i("GCM", regId + "\n");
                sendRegistrationToServer(regid);
            }
        }.execute(null, null, null);
    }

    private void sendRegistrationToServer(String token) {
        // Add custom impAPIListener.javalementation, as needed.
        try {
            Toast.makeText(MainActivity.this, "Token received for server: " + token, Toast.LENGTH_SHORT).show();
            APIJSONHandler apijsonHandler = new APIJSONHandler();
            apijsonHandler.registerGCMAPI(token, "hari.aarthika@gmail.com", "test", new APIListener() {
                @Override
                public void onStart() {

                }

                @Override
                public void onComplete(String response) {
                    Log.e(Finals.TAG, "Response: " + response);
                    startService(new Intent(MainActivity.this, MyGcmListenerService.class));
                }

                @Override
                public void onCompleteWithError(String error) {
                    Log.e(Finals.TAG, "Error: " + error);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
