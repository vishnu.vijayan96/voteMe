package in.projects.election.voteme.model;

import java.util.ArrayList;

public class ResponseMedia extends SimpleResponse{
	public Data data;

	public ResponseMedia(Data data, String status, String message) {
		super(status, message);
		this.data = data;
	}

	public class Data {
		ArrayList<MediaModel> media = new ArrayList<MediaModel>();

		public ArrayList<MediaModel> getMedia() {
			return media;
		}

		public void setMedia(ArrayList<MediaModel> media) {
			this.media = media;
		}
		
		
	}
}
