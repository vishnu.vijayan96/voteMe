package in.projects.election.voteme.db;

public interface DBListener {

	/**
	 * On success.
	 */
	public void onSuccess();

	/**
	 * On failure.
	 *
	 * @param reason
	 *            the reason
	 */
	public void onFailure(String reason);

}
