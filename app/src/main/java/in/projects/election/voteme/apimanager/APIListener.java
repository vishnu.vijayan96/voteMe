package in.projects.election.voteme.apimanager;

/**
 * Created by Pratheeja on 19/2/16.
 */
public interface APIListener {
    public void onStart();
    public void onComplete(String response);
    public void onCompleteWithError(String error);
}
