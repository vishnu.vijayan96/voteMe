package in.projects.election.voteme.Fragment;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.adapter.ContactListAdapter;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.model.ContactsListClass;
import in.projects.election.voteme.model.ContactsObject;

/**
 * Created by Pratheeja on 14/3/16.
 */
public class InviteFriendFragment extends Fragment implements View.OnClickListener{
    private View rootView;
    private FrameLayout containerId;

    Button btnShare;
    Button btnSendSMS,whatzappButton;
    ListView contactListview;
    ContactListAdapter contactAdapter;
    CheckBox chkSelectAll;
    ArrayList<ContactsObject> contactList = new ArrayList<ContactsObject>();
    public static ArrayList<Boolean> arrBoolean = new ArrayList<Boolean>();

    private ProgressBar mProgress;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatActivity actionBarActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        if (null != actionBar) {
            actionBar.setTitle(getResources().getString(R.string.title_invite_friends));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_invite_friends, container, false);
        containerId = (FrameLayout) this.getActivity().findViewById(R.id.container_body);
        bindControls();


        return rootView;
    }

    public void shareOnWhatzApp(){
        String msg = "";
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
         msg = Singletons.getInstance().getShare_app_text()+"";
        if(msg.equals(""))
            sendIntent.putExtra(Intent.EXTRA_TEXT,getString(R.string.sms_message));
        else
            sendIntent.putExtra(Intent.EXTRA_TEXT,msg);
        sendIntent.setType("text/plain");
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
    }



    private void bindControls(){
        chkSelectAll = (CheckBox) rootView.findViewById(R.id.check_select_all);
        btnSendSMS = (Button) rootView.findViewById(R.id.btn_contacts_sms);
        mProgress = (ProgressBar) rootView.findViewById(R.id.progressbar);
        whatzappButton=(Button) rootView.findViewById(R.id.whatzUpbutton);
        whatzappButton.setOnClickListener(this);
      //  btnShare = (Button) view.findViewById(R.id.btn_menu_action);
       // btnShare.setVisibility(View.VISIBLE);
        btnSendSMS.setOnClickListener(this);
       // btnShare.setOnClickListener(this);
        contactListview = (ListView) rootView.findViewById(R.id.contactlist);
        chkSelectAll.setChecked(true);
        contactListview.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int position, long arg3) {
                Log.i("ListViewTest","Item Click");
                Toast.makeText(getActivity(), "Clicked :"+position, Toast.LENGTH_LONG).show();
                // contactAdapter.notifyDataSetChanged();
                CheckBox chk = (CheckBox) view.findViewById(R.id.chk_list_item);
                ContactsObject bean = ContactsListClass.phoneList.get(position);
                if (bean.isSelected()) {
                    bean.setSelected(false);
                    chk.setChecked(false);
                } else {
                    bean.setSelected(true);
                    chk.setChecked(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    @Override
    public void onResume() {
        Utils.showProgress(getActivity(), "Loading...");
        readContacts();
        chkSelectAll.setChecked(true);
        setContactsChecked(true);
        contactListview.setAdapter(contactAdapter);
        contactAdapter.notifyDataSetChanged();
        chkSelectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean checked) {
               // Toast.makeText(getActivity(), "Checked :"+checked, Toast.LENGTH_LONG).show();
                if(checked){
                    setContactsChecked(true);
                    contactListview.setAdapter(contactAdapter);
                    contactAdapter.notifyDataSetChanged();
                }
                else{
                    setContactsChecked(false);
                    contactListview.setAdapter(contactAdapter);
                    contactAdapter.notifyDataSetChanged();
                }
            }
        });
        Utils.hideProgress();
        super.onResume();
    }

    private void setContactsChecked(boolean flag){
        Log.e(Finals.TAG, "Checked :"+flag+contactList.size());

        for (int i =0; i<ContactsListClass.phoneList.size(); i++){
            ContactsListClass.phoneList.get(i).setSelected(flag);
        }

    }
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_contacts_sms){
            getSelectedContacts();
        }else if(v.getId()== R.id.whatzUpbutton){
            // contactListview.setVisibility(View.GONE);
            // shareOnWhatzApp();
            shareData();
        }
        else{
            //contactListview.setVisibility(View.GONE);
            //share details
            shareData();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      //  readContacts();

    }

    private void sentSMS(String numbers) {
        String msg = "";
        Uri smsUri = Uri.parse("smsto:"+ numbers);
        Intent intent = new Intent(Intent.ACTION_VIEW, smsUri);
        if(msg.equals(""))
            intent.putExtra("sms_body", getString(R.string.sms_message));
        else
            intent.putExtra("sms_body", msg);

        startActivity(intent);
    }
    private void getSelectedContacts() {
        StringBuffer sb = new StringBuffer();

        for (ContactsObject bean : ContactsListClass.phoneList) {

            if (bean.isSelected()) {
                sb.append(bean.getNumber());
                sb.append(";");
            }
        }

        String s = sb.toString().trim();

        if (TextUtils.isEmpty(s)) {
            Toast.makeText(getActivity(), "Select atleast one Contact",
                    Toast.LENGTH_SHORT).show();
        } else {
            sentSMS(s);
            s = s.substring(0, s.length() - 1);
            Toast.makeText(getActivity(), "Selected Contacts : " + s,
                    Toast.LENGTH_SHORT).show();

        }

    }

    private ArrayList<ContactsObject> readContacts(){
      try {
          String name;
          String phoneNumber;
          String phoneImage;
          Cursor cursor = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
          while (cursor.moveToNext()) {
              name = cursor.getString(cursor
                      .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
              phoneNumber = cursor.getString(cursor
                      .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
              phoneImage = cursor.getString(cursor
                      .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
              ContactsObject cp = new ContactsObject();

              cp.setName(name);
              cp.setNumber(phoneNumber);
              cp.setImage(phoneImage);

              ContactsListClass.phoneList.add(cp);
          }
          //contactListview.setOnItemClickListener(ContactSelectedListener);
          Log.e("tag", "Size : " + contactList.size());
          contactAdapter = new ContactListAdapter(getActivity(), ContactsListClass.phoneList);
          contactListview.setAdapter(contactAdapter);
          mProgress.setVisibility(View.GONE);

      }catch(Exception e){
          e.printStackTrace();
          mProgress.setVisibility(View.GONE);
      }finally {
          return contactList;
      }

    }
    private void shareData(){
        String msg = "";
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        // Uri imagePath = Uri.parse(path);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT,getString(R.string.app_name));
        if(msg.equals("")) {
            String link=Singletons.getInstance().getShare_app_text();
            String shrMsg= getString(R.string.sms_message)+"\n"+link;
            shareIntent.putExtra(Intent.EXTRA_TEXT,shrMsg);
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.sms_message));
        }else
        {
            shareIntent.putExtra(Intent.EXTRA_TEXT,msg);
        }


        startActivity(Intent.createChooser(shareIntent, "Share using"));
    }
}
