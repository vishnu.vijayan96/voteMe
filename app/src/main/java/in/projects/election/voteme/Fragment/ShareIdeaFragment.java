package in.projects.election.voteme.Fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.activity.MainActivity;
import in.projects.election.voteme.activity.facebookListener;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.model.SimpleResponse;

public class ShareIdeaFragment extends Fragment implements  OnClickListener{
	//private LoginButton loginButton;
	//private CallbackManager callbackManager;

	private EditText edtTopic;
	private EditText edtIdea;
	private EditText edtEmail;
	private EditText edtPhone;
	private Button btnPost;
    private Button backButton;

	private View rootView;
	private FrameLayout containerId;
	private String fbUserId = "";
	private String fbauthToken = "";

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        // Add code to print out the key hash
        try {
            PackageInfo info =this.getActivity().getPackageManager().getPackageInfo(
                    "in.projects.election.candidate",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

		AppCompatActivity actionBarActivity = (AppCompatActivity) getActivity();
		ActionBar actionBar = actionBarActivity.getSupportActionBar();
		if (null != actionBar) {
			actionBar.setTitle(getResources().getString(R.string.title_share_idea));
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e(Finals.TAG, "onActivityResult.");
        super.onActivityResult(requestCode, resultCode, data);

    }
/*
    @Override
    public void fbdataReceived(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }*/

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		rootView = inflater.inflate(R.layout.fragment_share_idea, container, false);
		containerId = (FrameLayout) this.getActivity().findViewById(R.id.container_body);
        bindControls();
      //  callbackManager = ((MainActivity)getActivity()).getCallBackMAnager();
      //  ((MainActivity) getActivity()).setCallBackListener(this);
      /*  loginButton = (LoginButton) rootView.findViewById(R.id.login_button);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(Finals.TAG, "Login attempt success.");
                fbUserId = loginResult.getAccessToken().getUserId();
                fbauthToken = loginResult.getAccessToken().getToken();

            }

            @Override
            public void onCancel() {
                Log.e(Finals.TAG, "Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e(Finals.TAG, e.getMessage().toString());
                Log.e(Finals.TAG, "Login attempt failed.");
            }
        });*/

      //  callbackManager = CallbackManager.Factory.create();
        return rootView;
	}

	private void bindControls() {


		edtTopic = (EditText) rootView.findViewById(R.id.edt_topic);
		edtIdea  = (EditText) rootView.findViewById(R.id.edt_idea);
		edtEmail = (EditText) rootView.findViewById(R.id.edt_email);
		edtPhone = (EditText) rootView.findViewById(R.id.edt_phone);

		btnPost = (Button) rootView.findViewById(R.id.btn_post_idea);
		btnPost.setOnClickListener(this);
        backButton=(Button) rootView.findViewById(R.id.back_button);
        backButton.setOnClickListener(this);


	}

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }


    private boolean isValidEmail(String emailInput) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(emailInput);
        return matcher.matches();
    }
/*	public boolean isLoggedIn() {

		AccessToken accessToken = AccessToken.getCurrentAccessToken();
		if(accessToken != null)
			fbUserId = accessToken.getUserId();
		return accessToken != null;
	}*/
	@Override
	public void onClick(View v) {

		if(v.getId() == R.id.btn_post_idea){
			//fbShare();
			//isLoggedIn();
			if(edtTopic.getText().toString().trim().equals("") || edtIdea.getText().toString().trim().equals("") ||
                    edtEmail.getText().toString().trim().equals("") || edtPhone.getText().toString().trim().equals("")){
				Toast.makeText(getActivity(), R.string.mandatory_field_check, Toast.LENGTH_LONG).show();
			}/*else if(fbUserId.equals("") ) {
				Toast.makeText(getActivity(), "You must login with facebook for sharing data.", Toast.LENGTH_LONG).show();
            } */
            else if(!isValidEmail(edtEmail.getText().toString().trim())){
                Toast.makeText(getActivity(), R.string.email_validation, Toast.LENGTH_LONG).show();
            }else if(edtPhone.getText().toString().trim().length() > 10 || edtPhone.getText().toString().length() < 6){
                Toast.makeText(getActivity(), R.string.phone_validation, Toast.LENGTH_LONG).show();
            }else if(!isValidPhoneNumber(edtPhone.getText().toString().trim())){
                Toast.makeText(getActivity(), R.string.phone_validation, Toast.LENGTH_LONG).show();
            }else {
                shareIdeaToServer();
            }
        }else if(v.getId()==R.id.back_button){
                LoadMyconstitutionFragment();
        }
    }

    private void LoadMyconstitutionFragment() {
        if (getFragmentManager().getBackStackEntryCount() > 0 ){
            getFragmentManager().popBackStack();
        }
    }

    private void shareIdeaToServer() {
        APIJSONHandler apijsonHandler = new APIJSONHandler();
        apijsonHandler.shareIdeaAPI(Singletons.getInstance().getAuth_key(), edtTopic.getText().toString().trim(),
                edtIdea.getText().toString().trim(),edtEmail.getText().toString(),
                edtPhone.getText().toString(), new APIListener() {
                    @Override
                    public void onStart() {
						Utils.showProgress(getActivity(),"Loading...");
					}

                    @Override
                    public void onComplete(String response) {

                        try {
                            Gson gson = new Gson();
                            SimpleResponse responseIdea = gson.fromJson(response, SimpleResponse.class);
                            if (response != null) {
                                Toast.makeText(getActivity(), responseIdea.getMessage().toString(), Toast.LENGTH_LONG).show();
                            }
                           Utils.hideProgress();
                            LoadMandalamFragment();
                        } catch (Exception e){
							e.printStackTrace();
							Utils.hideProgress();
						}
					}

					@Override
					public void onCompleteWithError(String error) {
						Utils.hideProgress();
					}
				});

	}

    private void LoadMandalamFragment() {
        Fragment fragment = new MyConstitutionFragment();
        FragmentManager fragmentManager = this.getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerId.getId(), fragment);
        fragmentTransaction.commit();
    }


	private void fbShare(){
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("text/html");
		sharingIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>This is the text that will be shared.</p>"));
		startActivity(Intent.createChooser(sharingIntent,"Share using"));

	}
}