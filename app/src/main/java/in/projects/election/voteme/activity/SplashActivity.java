package in.projects.election.voteme.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.pushnotification.MyGcmListenerService;

public class SplashActivity extends Activity {

	int welcomeScreenDisplay = 1000;
	int wait = 0;
    GoogleCloudMessaging gcm;
    String regid;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash);
		
		Thread SplashScreen_tread = new Thread() {
			public void run() {
				try {
					super.run();
					while (wait < welcomeScreenDisplay) {
						sleep(100);
						wait += 100;

					}
				} catch (Exception e) {
					// TODO: handle exception
				} finally {
					Intent intent =  new Intent(SplashActivity.this,MainActivity.class);
					startActivity(intent);
					finish();
                    if (checkPlayServices()) {
                        //getGCMRegId();
                    }
				}

			}
		};
		SplashScreen_tread.start();

	}

	private boolean checkPlayServices() {
		GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
		int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (apiAvailability.isUserResolvableError(resultCode)) {
				apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
						.show();
			} else {
				Log.i(Finals.TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	public void getGCMRegId() {

		if (Utils.isInternetAvailable()){

			new AsyncTask<Void, Void, String>() {

				@Override
				protected String doInBackground(Void... params) {
					String msg = "";
					try {
						if (gcm == null) {
							gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
						}
						regid = gcm.register(getResources().getString(R.string.gcm_defaultSenderId));
						msg = "Device registered, registration ID=" + regid;
						Log.i("GCM", msg);

					} catch (IOException ex) {
						msg = "Error :" + ex.getMessage();
						Log.i("Error", msg);
						//	throw new RuntimeException(ex);

					}
					return regid;
				}

				@Override
				protected void onPostExecute(String regId) {
					Log.i("GCM", "Reg Id "+regId );
					if(!regId.equals("")) {
						sendRegistrationToServer(regid);

					}else{
						startService(new Intent(SplashActivity.this, MyGcmListenerService.class));
						Intent intent = new Intent(SplashActivity.this, MainActivity.class);
						startActivity(intent);
						finish();
					}
				}
			}.execute(null, null, null);

		}else{

			startService(new Intent(SplashActivity.this, MyGcmListenerService.class));
			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
			startActivity(intent);
			finish();

		}

	}

	private void sendRegistrationToServer(String token) {
		// Add custom impAPIListener.javalementation, as needed.
		try {
			//Toast.makeText(SplashActivity.this, "Token received for server: " + token, Toast.LENGTH_SHORT).show();
			APIJSONHandler apijsonHandler = new APIJSONHandler();
			apijsonHandler.registerGCMAPI(token, "hari.aarthika@gmail.com", "test", new APIListener() {
				@Override
				public void onStart() {

				}

				@Override
				public void onComplete(String response) {
					Log.e(Finals.TAG, "Response: " + response);
					if(response != null) {
						startService(new Intent(SplashActivity.this, MyGcmListenerService.class));
						Intent intent = new Intent(SplashActivity.this, MainActivity.class);
						startActivity(intent);
						finish();
					}else
						Utils.showInformationAlert(SplashActivity.this, "Netwrok Error. Please try again.");
				}

				@Override
				public void onCompleteWithError(String error) {

					Log.e(Finals.TAG, "Error: " + error);
					Utils.showInformationAlert(SplashActivity.this, getString(R.string.volley_server_error_msg));
				}
			});
		}catch (Exception e){
			e.printStackTrace();
		}

	}
}
