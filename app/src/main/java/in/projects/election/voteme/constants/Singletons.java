/**
 * 
 */
package in.projects.election.voteme.constants;


/**
 * @author Pratheeja Praveen
 *
 */
public class Singletons {
	private static Singletons self;
	private String notification_msg = "";
	private String auth_key = "";
	private String share_app_text= "";
	private Singletons() {
	}

	public static Singletons getInstance() {
		if (self == null) {
			self = new Singletons();
		}

		return self;
	}

	public String getAuth_key() {
		return auth_key;
	}

	public void setAuth_key(String auth_key) {
		this.auth_key = auth_key;
	}

    public String getShare_app_text() {
        return share_app_text;
    }

    public void setShare_app_text(String share_app_text) {
        this.share_app_text = share_app_text;
    }

	public String getNotification_msg() {
		return notification_msg;
	}

	public void setNotification_msg(String notification_msg) {
		this.notification_msg = notification_msg;
	}
}
