package in.projects.election.voteme.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.adapter.IdeaListAdapter;
import in.projects.election.voteme.adapter.SpeechListAdapter;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.db.DBConstants;
import in.projects.election.voteme.db.ElectionDatabase;
import in.projects.election.voteme.model.IdeaModel;
import in.projects.election.voteme.model.ResponseConstitutionComments;
import in.projects.election.voteme.model.ResponseSpeech;
import in.projects.election.voteme.model.SpeechModel;

public class SpeechFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
	private View rootView;
	private FrameLayout containerId;
	private ListView listSpeech;
	private SpeechListAdapter adapter = null;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar mProgress;
    private TextView txtNodata;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppCompatActivity actionBarActivity = (AppCompatActivity) getActivity();
		ActionBar actionBar = actionBarActivity.getSupportActionBar();
		if (null != actionBar) {
			actionBar.setTitle(getResources().getString(R.string.title_speech));
			actionBar.setDisplayHomeAsUpEnabled(true);
		}



    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		rootView = inflater.inflate(R.layout.fragment_speech, container, false);
		containerId = (FrameLayout) this.getActivity().findViewById(R.id.container_body);
		bindControls();
		return rootView;
	}

	private void bindControls(){
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(this);
        txtNodata = (TextView) rootView.findViewById(R.id.no_data_msg);
        mProgress = (ProgressBar) rootView.findViewById(R.id.progressbar);
        listSpeech = (ListView) rootView.findViewById(R.id.list_idea);

	}

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        getDataFromServer();
    }



	ArrayList<SpeechModel> SpeechList = new ArrayList<SpeechModel>();

	
	@Override
	public void onResume() {
		super.onResume();
        checkForSpeechInDB();
        if(SpeechList.size() <= 0)
          getDataFromServer();
        else{
            adapter = new SpeechListAdapter(getActivity(), SpeechList);
            listSpeech.setAdapter(adapter);
        }
	}
	
	private void getDataFromServer(){
        Utils.showProgress(getActivity(),"Loading...");
		 APIJSONHandler apijsonHandler = new APIJSONHandler();
         apijsonHandler.getSpeechDataAPI(Singletons.getInstance().getAuth_key(), new APIListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete(String response) {

                Log.e(Finals.TAG, "Json :"+response);
                try{
                    Gson gson = new Gson();
                    ResponseSpeech responseComments = gson.fromJson(response, ResponseSpeech.class);
                    if(responseComments != null){
                        SpeechList.clear();
                        SpeechList.addAll(responseComments.data.getSpeech());
                        adapter = new SpeechListAdapter(getActivity(), SpeechList);
                        listSpeech.setAdapter(adapter);

                        insertIntoDB(responseComments.data.getSpeech());
                    }else {
                        txtNodata.setVisibility(View.VISIBLE);
                    }
                   Utils.hideProgress();
                }catch(Exception e){
                    e.printStackTrace();
                    txtNodata.setVisibility(View.VISIBLE);
                    Utils.hideProgress();
                }
            }

            @Override
            public void onCompleteWithError(String error) {
                txtNodata.setVisibility(View.VISIBLE);
                Utils.hideProgress();
            }
        });

	}

    private void insertIntoDB(ArrayList<SpeechModel> speech){
        ElectionDatabase db = new ElectionDatabase(getActivity());
        db.clearTable(DBConstants.Tables.speechTable);
        db.insertSpeechData(speech);
    }

    private void checkForSpeechInDB(){
        ArrayList<SpeechModel> speechList = new ArrayList<SpeechModel>();
        ElectionDatabase db = new ElectionDatabase(getActivity());
        speechList = db.getSpeechData();
        if(speechList != null && speechList.size() > 0)
           this.SpeechList = speechList;

    }
}
