package in.projects.election.voteme.volley;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by com.tlt.mobile.hoppon on 17/11/15.
 */
public class CustomLoadingClass {

    private static ProgressDialog pDialog;

    public static void CustomLoadingShow(Context context, Boolean visibilty) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(visibilty);
        pDialog.show();
    }

    public static void CustomLoadingHide() {
        try {
            if(pDialog != null &&  pDialog.isShowing())
            pDialog.dismiss();
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
