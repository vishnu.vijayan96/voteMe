package in.projects.election.voteme.model;


import java.util.ArrayList;

import in.projects.election.voteme.constants.Singletons;

public class Profile {
	String authkey;
	String candidate_name;
	String about;
	//String profile_pic_url;
	String biography;
	String agitations;
	String fb_cover_bg;
	String title;
	private String AppShare_url;

	ArrayList<String> profile_pic_url = new ArrayList<String>();
	public String getAuthkey() {
		return authkey;
	}
	public void setAuthkey(String authkey) {
		this.authkey = authkey;
		Singletons.getInstance().setAuth_key(authkey);
	}
	public String getCandidate_name() {
		return candidate_name;
	}
	public void setCandidate_name(String candidate_name) {
		this.candidate_name = candidate_name;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}

	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
	public String getAgitations() {
		return agitations;
	}
	public void setAgitations(String agitations) {
		this.agitations = agitations;
	}
	public String getFb_cover_bg() {
		return fb_cover_bg;
	}
	public void setFb_cover_bg(String fb_cover_bg) {
		this.fb_cover_bg = fb_cover_bg;
	}

	public Profile(String candidate_name, String about, String biography, String agitations) {
		this.candidate_name = candidate_name;
		this.about = about;
		this.biography = biography;
		this.agitations = agitations;
	}

	public Profile() {

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



    public ArrayList<String> getProfile_pic_url() {
		return profile_pic_url;
	}

	public void setProfile_pic_url(ArrayList<String> profile_pic_url) {
		this.profile_pic_url = profile_pic_url;
	}

	public String getAppShare_url() {
		return AppShare_url;
	}

	public void setAppShare_url(String appShare_url) {
		AppShare_url = appShare_url;
	}
}
