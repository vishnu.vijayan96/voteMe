package in.projects.election.voteme.model;

import java.util.ArrayList;

public class ResponsePosters extends SimpleResponse{
	public Data data;

	public ResponsePosters(Data data, String status, String message) {
		super(status, message);
		this.data = data;
	}

	public class Data {
		ArrayList<Poster> posters = new ArrayList<Poster>();

		public ArrayList<Poster> getPosters() {
			return posters;
		}

		public void setPosters(ArrayList<Poster> posters) {
			this.posters = posters;
		}

		
	}
}
