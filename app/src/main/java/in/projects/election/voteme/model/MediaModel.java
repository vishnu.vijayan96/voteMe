package in.projects.election.voteme.model;

public class MediaModel {
	String _id;
	String media_title;
	String media_url;
	String media_type;

	public MediaModel(String media_title, String media_url, String media_type) {
		this.media_title = media_title;
		this.media_url = media_url;
		this.media_type = media_type;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getMedia_title() {
		return media_title;
	}

	public void setMedia_title(String media_title) {
		this.media_title = media_title;
	}

	public String getMedia_url() {
		return media_url;
	}

	public void setMedia_url(String media_url) {
		this.media_url = media_url;
	}

	public String getMedia_type() {
		return media_type;
	}

	public void setMedia_type(String media_type) {
		this.media_type = media_type;
	}
}
