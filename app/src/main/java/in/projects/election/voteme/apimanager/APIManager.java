package in.projects.election.voteme.apimanager;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.volley.AppController;

/**
 * Created by Pratheeja on 19/2/16.
 */
public class APIManager {
    Context apiContext;

    public APIManager() {
    }

    public APIManager(Context context) {
        this.apiContext = context;

    }

    /**
     * Common GET API call for communicating with server
     * @param jsonObject - json object to be given to server
     * @param url - url for server communication
     * @param listener - API Listener interface
     */
    public void sendGETAPI(JSONObject jsonObject , String url, final APIListener listener){
        try {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                        listener.onComplete(response.toString());
                }
                }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    VolleyLog.d("", "Error: " + error.getMessage());
                   listener.onCompleteWithError(error.getMessage());

                }
            }) {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    /*headers.put("Content-Type", "application/json" );*/
                   /*headers.put( "charset", "utf-8");*/
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5000,
                    2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Common API call for communicating with server
     * @param jsonObject - json object to be given to server
     * @param url - url for server communication
     * @param listener - API Listener interface
     */
    public void sendPOSTAPI(JSONObject jsonObject , String url, final APIListener listener){
        Log.e(Finals.TAG,"Request :"+jsonObject.toString());
        try {
            listener.onStart();
            
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e(Finals.TAG,"Response :"+response.toString());
                    listener.onComplete(response.toString());
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                   // listener.onCompleteWithError("Error: " +error.getMessage().toString());
                    listener.onComplete(null);
                }
            })
            {
                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    /*headers.put("Content-Type", "application/json" );*/
                   /*headers.put( "charset", "utf-8");*/
                    return headers;
                }
            };
            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(5000,
                    2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        }catch(Exception e){
            e.printStackTrace();
        }
    }



    }
