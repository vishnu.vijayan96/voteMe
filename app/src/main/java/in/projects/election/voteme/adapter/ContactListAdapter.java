package in.projects.election.voteme.adapter;
import java.util.ArrayList;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import in.projects.election.voteme.R;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.model.ContactsObject;

public class ContactListAdapter extends BaseAdapter{

	Activity activity;
	ArrayList<ContactsObject> contactList = new ArrayList<ContactsObject>();
	ViewHolder viewholder;
	LayoutInflater mInflator;

	public ContactListAdapter(Activity activity, ArrayList<ContactsObject> contactList) {
		super();
		this.activity = activity;
		this.contactList = contactList;
		mInflator = LayoutInflater.from(activity);
	}

	@Override
	public int getCount() {
		return contactList.size();
	}

	@Override
	public Object getItem(int position) {
		return contactList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		try{
			if(view == null){
				view = mInflator.inflate(R.layout.listitem_contacts, null);
				viewholder = new ViewHolder();
				viewholder.txtName = (TextView) view.findViewById(R.id.txt_contact_name);
				viewholder.txtNumber = (TextView) view.findViewById(R.id.txt_contact_number);
				viewholder.chkName = (CheckBox) view.findViewById(R.id.chk_list_item);

				view.setTag(viewholder);
				//view.setTag(R.id.txt_contact_name, viewholder.txtName);
				//view.setTag(R.id.txt_contact_number, viewholder.txtNumber);
				//view.setTag(position);

                viewholder.chkName
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton vw,
                                                     boolean isChecked) {

                            int getPosition = (Integer) vw.getTag();
                            contactList.get(getPosition).setSelected(vw.isChecked());

                        }
                    });

			}else{
				viewholder = (ViewHolder) view.getTag();
                viewholder.chkName.getTag(position);
			}
            viewholder.chkName.setTag(position);
            Log.e(Finals.TAG, "Selection : "+position+contactList.get(position).isSelected());
			viewholder.txtName.setText(contactList.get(position).getName());
			viewholder.txtNumber.setText(contactList.get(position).getNumber());
			viewholder.chkName.setChecked(contactList.get(position).isSelected());
		}catch (Exception e){
			e.printStackTrace();
		}
		return view;
	}

	public class ViewHolder{
		TextView txtName;
		TextView txtNumber;
		CheckBox chkName;
	}
}