package in.projects.election.voteme.Fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.adapter.CampaignListAdapter;
import in.projects.election.voteme.adapter.ProfileImageAdapter;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.db.DBConstants;
import in.projects.election.voteme.db.ElectionDatabase;
import in.projects.election.voteme.model.Profile;
import in.projects.election.voteme.model.ResponseProfile;

/**
 * Created by Pratheeja on 14/3/16.
 */
public class ProfileFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View rootView;
    private FrameLayout containerId;
    private boolean doubleBackToExitPressedOnce = false;

    private TextView txtCandidateTitle;
    private TextView txtCandidateName;
    private TextView txtCandidateAbout;
    private TextView txtCandidateBiography;
    private TextView txtCandidateAgitations;

    private SwipeRefreshLayout swipeRefreshLayout;


    private RecyclerView imageRecyclerList;
    private File proFile;
    private LinearLayoutManager mLayoutManager;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        proFile= new File(Finals.PROFILE_PATH);
        proFile.mkdirs();
        AppCompatActivity actionBarActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        if (null != actionBar) {
            actionBar.setTitle(getResources().getString(R.string.title_profile));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onRefresh() {
        getProfileFromServer();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        containerId = (FrameLayout) this.getActivity().findViewById(R.id.container_body);
        bindControls();

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here

        }
        return rootView;
    }

    private void bindControls(){
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(this);
        txtCandidateTitle = (TextView) rootView.findViewById(R.id.txt_candidate_title);
        txtCandidateName = (TextView) rootView.findViewById(R.id.candidate_name);
        txtCandidateAbout = (TextView) rootView.findViewById(R.id.profile_about);
        txtCandidateBiography = (TextView) rootView.findViewById(R.id.profile_biography);
        txtCandidateAgitations = (TextView) rootView.findViewById(R.id.profile_agitations);
       // imgCandidatePhoto = (ImageView) rootView.findViewById(R.id.img_candidate_photo);
       //
        imageRecyclerList = (RecyclerView) rootView.findViewById(R.id.image_recyclerview);
        mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
        imageRecyclerList.setLayoutManager(mLayoutManager);


    }


   @Override
    public void onResume() {
      // getProfileFromServer();

       ElectionDatabase db = new ElectionDatabase(getActivity());
       Profile profile = db.getProfileData();
       if(profile == null ) {
           File dir = new File(Finals.PROFILE_PATH);
           if (dir.isDirectory())
           {
               String[] children = dir.list();
               for (int i = 0; i < children.length; i++)
               {
                   new File(dir, children[i]).delete();
               }
           }
           getProfileFromServer();
       }else
           //mProgress.setVisibility(View.GONE);
            fillProfileData(profile);


        super.onResume();
    }

    ResponseProfile responseProfile = null;
    private void getProfileFromServer(){
        Utils.showProgress(getActivity(), "Loading...");
        APIJSONHandler apijsonHandler = new APIJSONHandler();
        Utils.showProgress(getActivity(), "Loading...");
        apijsonHandler.getProfileAPI(Singletons.getInstance().getAuth_key(), new APIListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete(String response) {
                Log.e(Finals.TAG, "Response :"+response);

                try{
                    if(response != null){
                        Gson gson = new Gson();
                        responseProfile = gson.fromJson(response, ResponseProfile.class);
                        Singletons.getInstance().setAuth_key(responseProfile.data.getAuthkey());
                        Singletons.getInstance().setShare_app_text(responseProfile.data.getApp_share_url());
                        new DownloadImageTask().execute();

                    }


                }catch(Exception e){
                    e.printStackTrace();
                    Utils.hideProgress();
                }
            }

            @Override
            public void onCompleteWithError(String error) {
                Utils.hideProgress();
            }
        });


    }

    private void fillProfileData(Profile responseProfile) {
       // txtCandidateTitle.setText(responseProfile.getTitle());

        txtCandidateName.setText(responseProfile.getCandidate_name());
        txtCandidateAbout.setText(responseProfile.getAbout());
        txtCandidateAgitations.setVisibility(View.GONE);
        txtCandidateBiography.setVisibility(View.GONE);
//        txtCandidateAgitations.setText(responseProfile.getAgitations());
//        txtCandidateBiography.setText(responseProfile.getBiography());

        ArrayList<Bitmap> urls = new ArrayList<Bitmap>();
        urls = getAllImageFiles();
        if (urls.size()>0){
            ProfileImageAdapter profileadapter = new ProfileImageAdapter(getActivity(),urls);
            imageRecyclerList.setAdapter(profileadapter);
        }

    }


    private ArrayList<Bitmap> getAllImageFiles(){
        ArrayList<Bitmap> drawablesId = new ArrayList<Bitmap>();
        File sdDir = new File(Finals.PROFILE_PATH);
        File[] sdDirFiles = sdDir.listFiles();
        if (sdDirFiles!=null){
            for (int i=0; i < sdDirFiles.length; i++)
            {
                Log.d("Files", "FileName:" + sdDirFiles[i].getName());
                Bitmap bitmap = BitmapFactory.decodeFile(sdDirFiles[i].getAbsolutePath());
                drawablesId.add(bitmap);
            }
        }

//            for(File singleFile : sdDirFiles){
//                Bitmap bitmap = BitmapFactory.decodeFile(singleFile.getAbsolutePath());
//                drawablesId.add(bitmap);
//
//            }

        return drawablesId;
    }

    private void insertProfileintoDb(Profile responseProfile){
        try {
            //Utils.DeleteRecursive(new File(Finals.PROFILE_PATH));
            ElectionDatabase database = new ElectionDatabase(getActivity());
            database.clearTable(DBConstants.Tables.profileTable);
            database.createProfile(responseProfile);
            database.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }



    /*
    Bitmap profileBitmap = null;
    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            Bitmap defaultIcon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.election_icon);
            return defaultIcon;
        }
    }

/*    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
               // InputStream in = new java.net.URL(urldisplay).openStream();
                URL url = new URL(urldisplay);
                Log.e(Finals.TAG, "Url : "+urldisplay);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                mIcon11 = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            profileBitmap = result;
            bmImage.setImageBitmap(result);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            profileBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte imageInByte[] = stream.toByteArray();
            Log.e(Finals.TAG, "Image in bytes :"+imageInByte.toString());
            Profile profile = new Profile(imageInByte, responseProfile.data.getCandidate_name(),
                    responseProfile.data.getAbout(), responseProfile.data.getBiography(), responseProfile.data.getAgitations());
            setProfilePic(profile);
            insertProfileintoDb(profile);
            fillProfileData(profile);
            mProgress.setVisibility(View.GONE);
        }
    }
*/



    private class DownloadImageTask extends AsyncTask<Void, Void, String> {
        ArrayList<byte[]> imageArray = new ArrayList<byte[]>();


        public void writeBitmaptoFile(Bitmap bmp,int position) throws IOException {
                File file = new File(Finals.PROFILE_PATH, responseProfile.data.getCandidate_name()+position+".jpg");
          //  getActivity().sendBroadcast(new Intent(
                  //  Intent.ACTION_MEDIA_MOUNTED,
                    //Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                FileOutputStream out = new FileOutputStream(file.getAbsoluteFile());
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
         }
         protected String doInBackground(Void... urls) {
            int arraySize = responseProfile.data.getProfile_pic_url().size();
            int currentPos = 0;
            File file;
            ArrayList<String> urllist=responseProfile.data.getProfile_pic_url();
            while(arraySize > currentPos) {
                String urldisplay = urllist.get(currentPos);
                Bitmap mIcon11 = null;
                try {
                    // InputStream in = new java.net.URL(urldisplay).openStream();

                    URL url = new URL(urldisplay);
                    Log.e(Finals.TAG, "Url : " + urldisplay);
                    //this will be used to write the downloaded data into the file we created


                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    writeBitmaptoFile(bmp,currentPos);

//



                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                currentPos++;
            }


            return "success";
        }

        protected void onPostExecute(String result) {
            Log.e(Finals.TAG,"onPostExecute");
            Utils.hideProgress();
            Profile profile = new Profile( responseProfile.data.getCandidate_name(),
                    responseProfile.data.getAbout(), responseProfile.data.getBiography(), responseProfile.data.getAgitations());

            insertProfileintoDb(profile);
            fillProfileData(profile);
        }
    }
}
