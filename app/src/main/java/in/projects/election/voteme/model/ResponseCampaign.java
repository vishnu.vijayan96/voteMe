package in.projects.election.voteme.model;

import java.util.ArrayList;

public class ResponseCampaign extends SimpleResponse{
	public Data data;

	public ResponseCampaign(Data data, String status, String message) {
		super(status, message);
		this.data = data;
	}

	public class Data {
		ArrayList<CampaignModel> campaign = new ArrayList<CampaignModel>();

		public ArrayList<CampaignModel> getCampaigns() {
			return campaign;
		}

		public void setCampaigns(ArrayList<CampaignModel> campaign) {
			this.campaign = campaign;
		}
	}
}
