package in.projects.election.voteme.Fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.adapter.CampaignListAdapter;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.db.DBConstants;
import in.projects.election.voteme.db.ElectionDatabase;
import in.projects.election.voteme.model.CampaignModel;
import in.projects.election.voteme.model.ResponseCampaign;

/**
 * Created by Pratheeja on 14/3/16.
 */
public class CampaignUpdatesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View rootView;
    private FrameLayout containerId;
    ListView campaignListView = null;
    ArrayList<CampaignModel> campaignList = new ArrayList<CampaignModel>();
    CampaignListAdapter adapter = null;

    private TextView txtNoData;
    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatActivity actionBarActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        if (null != actionBar) {
            actionBar.setTitle(getResources().getString(R.string.title_campaign_updates));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_campaign, container, false);
        containerId = (FrameLayout) this.getActivity().findViewById(R.id.container_body);
        bindControls();
        return rootView;
    }

    @Override
    public void onRefresh() {
        getDataFromServer();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void bindControls(){
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(this);
        txtNoData = (TextView) rootView.findViewById(R.id.txt_nodata_msg);
        campaignListView = (ListView) rootView.findViewById(R.id.list_campaign_updates);
        campaignListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int arg2,
                                    long arg3) {


            }
        });
    }

    @Override
    public void onResume() {
        checkForCampaignDatainDB();
        if(campaignList.size() <= 0)
            getDataFromServer();
        else
            adapter = new CampaignListAdapter(getActivity(), campaignList);
            campaignListView.setAdapter(adapter);
        super.onResume();
    }

    private void getDataFromServer(){
        APIJSONHandler apijsonHandler = new APIJSONHandler();
        apijsonHandler.campaignUpdateAPI(Singletons.getInstance().getAuth_key(), new APIListener() {
            @Override
            public void onStart() {
                Utils.showProgress(getActivity(),"Loading...");
            }

            @Override
            public void onComplete(String response) {
                Log.e(Finals.TAG, "Json :"+response);

                try{

                    Gson gson = new Gson();
                    ResponseCampaign responseMedia = gson.fromJson(response, ResponseCampaign.class);
                    if(responseMedia != null){
                        txtNoData.setVisibility(View.GONE);
                        campaignList.clear();
                        campaignList.addAll(responseMedia.data.getCampaigns());
                        insertIntoDB(campaignList);
                        adapter = new CampaignListAdapter(getActivity(), campaignList);
                        campaignListView.setAdapter(adapter);
                    }else
                        txtNoData.setVisibility(View.VISIBLE);
                 Utils.hideProgress();
                }catch(Exception e){
                    e.printStackTrace();
                    Utils.hideProgress();
                    txtNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCompleteWithError(String error) {
                Utils.hideProgress();
                txtNoData.setVisibility(View.VISIBLE);
            }
        });

    }

    private void insertIntoDB(ArrayList<CampaignModel> campaign){
        ElectionDatabase db = new ElectionDatabase(getActivity());
        db.clearTable(DBConstants.Tables.campaignTable);
        db.insertCampaignData(campaign);
    }

    private void checkForCampaignDatainDB(){
        ArrayList<CampaignModel> campaignList = new ArrayList<CampaignModel>();
        ElectionDatabase db = new ElectionDatabase(getActivity());
        campaignList = db.getCampaignData();
        if(campaignList != null && campaignList.size() > 0)
            this.campaignList = campaignList;

    }



}
