package in.projects.election.voteme.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.projects.election.voteme.R;
import in.projects.election.voteme.model.IdeaModel;
import in.projects.election.voteme.model.SpeechModel;

public class SpeechListAdapter extends BaseAdapter{
	Activity adapterContext;
	ArrayList<SpeechModel> speechList = new ArrayList<SpeechModel>();
	ViewHolder viewholder;
	LayoutInflater mInflator;
	public SpeechListAdapter(Activity context, ArrayList<SpeechModel> data){
		speechList = data;
		adapterContext = context;
		mInflator = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return speechList.size();
	}

	@Override
	public Object getItem(int pos) {
		return speechList.get(pos);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		try{
			if(view == null){
				view = mInflator.inflate(R.layout.listitem_ideas, null);
				viewholder = new ViewHolder();
				viewholder.txtTitle = (TextView) view.findViewById(R.id.txt_idea_title);
				viewholder.txtDescription = (TextView) view.findViewById(R.id.txt_idea_description);
				view.setTag(viewholder);
			}else{
				viewholder = (ViewHolder) view.getTag();
			}
			
			viewholder.txtTitle.setText(speechList.get(position).getTitle());
			viewholder.txtDescription.setText(speechList.get(position).getDescription());
		}catch (Exception e){
			e.printStackTrace();
		}
		return view;
	}

	public class ViewHolder{
		TextView txtTitle;
		TextView txtDescription;
	}
}






	