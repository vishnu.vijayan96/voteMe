package in.projects.election.voteme.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import in.projects.election.voteme.R;

/**
 * Created by tltvmd15 on 22/3/16.
 */
public class ProfileImageAdapter extends RecyclerView.Adapter<ProfileImageAdapter.MyViewHolder> {
    ArrayList<Bitmap> data = new ArrayList<Bitmap>();
    private LayoutInflater inflater;
    private Context context;

    public ProfileImageAdapter(Context context,  ArrayList<Bitmap> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.listitem_profile_image, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        holder.imgProfile = (ImageView)view.findViewById(R.id.img_item_profile);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       /* byte[] url  = data.get(position);
         Log.d("Menu Drawer","Menu icon:"+url);
       // holder.imgProfile.setImageBitmap(url);
        ByteArrayInputStream imageStream = new ByteArrayInputStream(url);
        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
        if(theImage == null)
            theImage = BitmapFactory.decodeResource(context.getResources(),R.drawable.election_icon);*/
        holder.imgProfile.setImageBitmap(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProfile;
        public MyViewHolder(View itemView) {
            super(itemView);

        }
    }

}
