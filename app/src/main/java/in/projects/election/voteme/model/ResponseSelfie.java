package in.projects.election.voteme.model;

public class ResponseSelfie extends SimpleResponse{
	public Data data;

	public ResponseSelfie(Data data, String status, String message) {
		super(status, message);
		this.data = data;
	}

	public class Data {
		String image;
        byte[] banner_image;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public byte[] getBanner_image() {
            return banner_image;
        }

        public void setBanner_image(byte[] banner_image) {
            this.banner_image = banner_image;
        }
    }
}
