package in.projects.election.voteme.Utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by tltvmd15 on 24/3/16.
 */
public class Progress{
        private static ProgressDialog progress = null;

        public static void showProgress(Context context, String message){
            Progress.hideProgress();
            progress = new ProgressDialog(context);
            progress.setMessage(message);
            progress.setCancelable(false);
            progress.setCanceledOnTouchOutside(false);
            progress.show();
        }

        public static void setProgressText(String message){
            if(null != progress && progress.isShowing()){
                progress.setMessage(message);
            }
        }

        public static void hideProgress(){
            if(null != progress){
                if(progress.isShowing())
                    progress.dismiss();

                progress = null;
            }
        }
}

