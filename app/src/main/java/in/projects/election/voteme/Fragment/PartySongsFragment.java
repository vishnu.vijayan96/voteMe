package in.projects.election.voteme.Fragment;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import in.projects.election.voteme.R;
import in.projects.election.voteme.Utils.Utils;
import in.projects.election.voteme.adapter.MediaListAdapter;
import in.projects.election.voteme.apimanager.APIJSONHandler;
import in.projects.election.voteme.apimanager.APIListener;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.constants.Singletons;
import in.projects.election.voteme.db.DBConstants;
import in.projects.election.voteme.db.ElectionDatabase;
import in.projects.election.voteme.model.MediaModel;
import in.projects.election.voteme.model.Profile;
import in.projects.election.voteme.model.ResponseMedia;

/**
 * Created by tltvmd15 on 14/3/16.
 */
public class PartySongsFragment extends Fragment {
    private View rootView;
    private FrameLayout containerId;
    ListView mediaListView = null;
    ArrayList<MediaModel> mediaList = new ArrayList<MediaModel>();
    MediaListAdapter adapter = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatActivity actionBarActivity = (AppCompatActivity) getActivity();
        ActionBar actionBar = actionBarActivity.getSupportActionBar();
        if (null != actionBar) {
            actionBar.setTitle(getResources().getString(R.string.title_party_songs));
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = inflater.inflate(R.layout.fragment_party_songs, container, false);
        containerId = (FrameLayout) this.getActivity().findViewById(R.id.container_body);
        bindControls();
        return rootView;
    }

    private void bindControls(){
        mediaListView = (ListView) rootView.findViewById(R.id.list_party_songs);
        mediaListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int arg2,
                                    long arg3) {


            }
        });
    }

    @Override
    public void onResume() {
        File file = new File(Finals.MEDIA_PATH);
        if (! file.exists()){
            file.mkdirs();
        }

        checkForMediasInDB();
        if(mediaList.size() <= 0)
            if (Utils.isInternetAvailable()){
                getDataFromServer();
            }else{
                getFragmentManager().popBackStack();
                Utils.showInformationAlert(getContext(),"Check your internet settings");

            }

        else{
            adapter = new MediaListAdapter(getActivity(), mediaList);
            mediaListView.setAdapter(adapter);
        }

        super.onResume();
    }
    ResponseMedia responseMedia = null;
    private void getDataFromServer(){
        Utils.showProgress(getActivity(),"Downloading Progress...");
        APIJSONHandler apijsonHandler = new APIJSONHandler();
        apijsonHandler.mediaDownlodAPI(Singletons.getInstance().getAuth_key(), new APIListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete(String response) {
                Log.e(Finals.TAG, "Json :"+response);

                try{

                    Gson gson = new Gson();
                    responseMedia = gson.fromJson(response, ResponseMedia.class);
                    if(responseMedia != null){
                        mediaList.clear();
                        mediaList.addAll(responseMedia.data.getMedia());
                        new DownloadFileTask().execute();
                    }

                }catch(Exception e){
                    e.printStackTrace();
                    Utils.hideProgress();
                }
            }

            @Override
            public void onCompleteWithError(String error) {
                Utils.hideProgress();
            }
        });

    }

    private ArrayList<String> getAllAudioFiles(){
        ArrayList<String> musicId = new ArrayList<String>();
        File sdDir = new File(Finals.MEDIA_PATH);
        File[] sdDirFiles = sdDir.listFiles();
        for(File singleFile : sdDirFiles){
            musicId.add(singleFile.getAbsolutePath());

        }
        return musicId;
    }

    private void insertIntoDB(ArrayList<MediaModel> media){
        ElectionDatabase db = new ElectionDatabase(getActivity());
        db.clearTable(DBConstants.Tables.mediaTable);
      //  Utils.DeleteRecursive(new File(Finals.MEDIA_PATH));
        db.insertMediaData(media);
    }



    private void checkForMediasInDB(){
        ArrayList<MediaModel> mediaList = new ArrayList<MediaModel>();
        ElectionDatabase db = new ElectionDatabase(getActivity());
        mediaList = db.getMediaData();
        if(mediaList != null && mediaList.size() > 0)
            this.mediaList = mediaList;

    }

    private class DownloadFileTask extends AsyncTask<Void, Void, String> {
        ArrayList<byte[]> imageArray = new ArrayList<byte[]>();

        protected String doInBackground(Void... urls) {
            int arraySize = responseMedia.data.getMedia().size();
            int currentPos = 0;
            int count;String urldisplay= "";
            while(arraySize > currentPos) {
                count = 0;
                if(responseMedia.data.getMedia().get(currentPos).getMedia_type().equalsIgnoreCase("audio")) {
                    urldisplay = responseMedia.data.getMedia().get(currentPos).getMedia_url();
                    Log.i("File", "File name " + urldisplay);
                    try {

                        URL url = new URL(urldisplay);
                        URLConnection conexion = url.openConnection();
                        conexion.connect();
                        // this will be useful so that you can show a tipical 0-100% progress bar
                        int lenghtOfFile = conexion.getContentLength();

                        // downlod the file
                        InputStream input = new BufferedInputStream(url.openStream());
                        String path = Finals.MEDIA_PATH + currentPos +
                                responseMedia.data.getMedia().get(currentPos).getMedia_title() + ".mp3";
                        OutputStream output = new FileOutputStream(path);
                        responseMedia.data.getMedia().get(currentPos).setMedia_url(path);
                        byte data[] = new byte[lenghtOfFile];

                        long total = 0;

                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            output.write(data, 0, count);
                        }

                        output.flush();
                        output.close();
                        input.close();

                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }
                }

                currentPos++;
            }


            return "success";
        }

        protected void onPostExecute(String result) {
            Log.e(Finals.TAG,"onPostExecute");
            adapter = new MediaListAdapter(getActivity(), mediaList);
            mediaListView.setAdapter(adapter);
            insertIntoDB(responseMedia.data.getMedia());
            Utils.hideProgress();

        }
    }
}
