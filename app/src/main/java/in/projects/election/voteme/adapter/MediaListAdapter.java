package in.projects.election.voteme.adapter;


import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import in.projects.election.voteme.R;
import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.model.MediaModel;

public class MediaListAdapter extends BaseAdapter{
	Activity adapterContext;
	ArrayList<MediaModel> mediaList = new ArrayList<MediaModel>();
	ViewHolder viewholder;
	LayoutInflater mInflator;
	public MediaListAdapter (Activity context, ArrayList<MediaModel> data){
		mediaList = data;
		adapterContext = context;
		mInflator = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return mediaList.size();
	}

	@Override
	public Object getItem(int pos) {
		return mediaList.get(pos);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		try{
			if(view == null){
				view = mInflator.inflate(R.layout.listitem_party_songs, null);
				viewholder = new ViewHolder();
				viewholder.imgPlay = (ImageView) view.findViewById(R.id.img_item_icon);
				viewholder.txtTitle = (TextView) view.findViewById(R.id.txt_item_title);
                viewholder.txtPath = (TextView) view.findViewById(R.id.txt_item_path);

				viewholder.imgPlay.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						//call download function here.
						if(mediaList.get(position).getMedia_type().equalsIgnoreCase("video")){
							//choose intent
                            Log.e(Finals.TAG,mediaList.get(position).getMedia_url());
                            adapterContext.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(mediaList.get(position).getMedia_url())));
						}else{
                           // final Uri uri=Uri.parse(mediaList.get(position).getMedia_url());
                           // adapterContext.startActivity(new Intent(Intent.ACTION_VIEW, uri));
                          /*  Intent play = new Intent(Intent.ACTION_VIEW);
                            play.setData(Uri.fromFile(new File("/path/to/file")));
                            // create chooser for that intent
                            try {
                                Intent i = Intent.createChooser(play, "Play Music");
                                adapterContext.startActivity(i);
                            } catch(ActivityNotFoundException ex) {
                                // if no app handles it, do nothing
                            }*/
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            File file = new File(mediaList.get(position).getMedia_url());
                            intent.setDataAndType(Uri.fromFile(file), "audio/*");
                            adapterContext.startActivity(intent);
                        }

					}
				});
				view.setTag(viewholder);
			}else{
				viewholder = (ViewHolder) view.getTag();
			}
            if(mediaList.get(position).getMedia_type().equalsIgnoreCase("audio")) {
                viewholder.txtTitle.setText(mediaList.get(position).getMedia_title());
                       // viewholder.txtPath.setText(mediaList.get(position).getMedia_url());
                Log.e(Finals.TAG,"Url "+ mediaList.get(position).getMedia_url());
            }else
			    viewholder.txtTitle.setText(mediaList.get(position).getMedia_title());
		}catch (Exception e){
			e.printStackTrace();
		}
		return view;
	}

	public class ViewHolder{
		ImageView imgPlay;
		TextView txtTitle;
        TextView txtPath;
	}
}






	