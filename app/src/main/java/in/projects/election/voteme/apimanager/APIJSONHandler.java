package in.projects.election.voteme.apimanager;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import in.projects.election.voteme.constants.Finals;

/**
 * Created by Pratheeja on 19/2/16.
 */


public class APIJSONHandler {
    APIManager manager ;
    public APIJSONHandler() {

    }

    /**
     * Retrieves order history for a particular customer from server
     * @param apiListener - API Listener interface
     */
    public void getProfileAPI(String token,APIListener apiListener){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("auth_key", token);
            manager = new APIManager();
            manager.sendPOSTAPI(jsonObject, Finals.GET_PROFILE,apiListener);
        }catch(Exception e){
            e.printStackTrace();
        }

    }


    public void getMandalamDataAPI(String token,APIListener apiListener){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("auth_key", token);
            manager = new APIManager();
            manager.sendPOSTAPI(jsonObject, Finals.MY_CONSTIUTION ,apiListener);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void getSpeechDataAPI(String token,APIListener apiListener){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("auth_key", token);
            manager = new APIManager();
            manager.sendPOSTAPI(jsonObject, Finals.API_SPEECH ,apiListener);
        }catch(Exception e){
            e.printStackTrace();
        }

    }


    public void shareIdeaAPI(String token,String topic, String description, String email, String phone, APIListener listener){
        try{
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("auth_key", token);
            jsonParams.put("topic", topic);
            jsonParams.put("description", description);
            jsonParams.put("email", email);
            jsonParams.put("phone", phone);
            Log.e(Finals.TAG, "Share :"+jsonParams.toString());
            manager = new APIManager();
            manager.sendPOSTAPI(jsonParams, Finals.SHARE_IDEA ,listener);

        }catch(Exception e){
            e.printStackTrace();
        }

    }
    public void mediaDownlodAPI(String token, APIListener listener) {

        JSONObject jsonParams = new JSONObject();

        try {

            jsonParams.put("auth_key", token);

            manager = new APIManager();
            manager.sendPOSTAPI(jsonParams, Finals.API_SONGS_VIDEOS,listener);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void downlaodPosterAPI(String token, APIListener listener){
        try{
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("auth_key", token);

            manager = new APIManager();
            manager.sendPOSTAPI(jsonParams, Finals.API_DOWNLOAD_POSTER ,listener);

        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void campaignUpdateAPI(String token, APIListener listener) {

        JSONObject jsonParams = new JSONObject();

        try {

            jsonParams.put("auth_key", token);

            manager = new APIManager();
            manager.sendPOSTAPI(jsonParams, Finals.API_CAMPAIGN_UPDATE,listener);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void imageUploadAPI(String token, String image,APIListener listener) {

        JSONObject jsonParams = new JSONObject();

        try {

            jsonParams.put("auth_key", token);
            jsonParams.put("selfie_pic", image);
            manager = new APIManager();
            manager.sendPOSTAPI(jsonParams, Finals.API_SELFIE,listener);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void registerGCMAPI(String token, String email, String name, APIListener listener){
        JSONObject jsonParams = new JSONObject();
        String regUrl = Finals.API_REGISTER_GCM+token;
        try {
            jsonParams.put("email", email);
            jsonParams.put("name", "rain Test");
            jsonParams.put("regId", token);

            Log.e(Finals.TAG, " request: " + jsonParams.toString());
            manager = new APIManager();
            manager.sendGETAPI(jsonParams, regUrl,listener);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
