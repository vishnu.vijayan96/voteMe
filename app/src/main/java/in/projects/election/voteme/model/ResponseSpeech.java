package in.projects.election.voteme.model;

import java.util.ArrayList;

public class ResponseSpeech extends SimpleResponse{
	public Data data;

	public ResponseSpeech(Data data, String status, String message) {
		super(status, message);
		this.data = data;
	}

	public class Data {
		ArrayList<SpeechModel> speech = new ArrayList<SpeechModel>();

		public ArrayList<SpeechModel> getSpeech() {
			return speech;
		}

		public void setSpeech(ArrayList<SpeechModel> speech) {
			this.speech = speech;
		}
	}
}
