package in.projects.election.voteme.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.projects.election.voteme.R;
/**
 * Created by tltvmd15 on 10/3/16.
 */
public class test extends Activity {
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = (ImageView) findViewById(R.id.image);

       // combineImages(largeIcon1,largeIcon2);
        //image.setImageBitmap(overlay(largeIcon1,largeIcon2));
        takePicture();
    }

    public Bitmap combineImages(Bitmap c, Bitmap s) { // can add a 3rd parameter 'String loc' if you want to save the new image - left some code to do that at the bottom
        Bitmap cs = null;

        int width, height = 0;

        if(c.getWidth() > s.getWidth()) {
            width = c.getWidth() + s.getWidth();
            height = c.getHeight();
        } else {
            width = s.getWidth() + s.getWidth();
            height = c.getHeight();
        }

        cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas comboImage = new Canvas(cs);

        comboImage.drawBitmap(c, 0f, 0f, null);
        comboImage.drawBitmap(s, c.getWidth(), 0f, null);

        // this is an extra bit I added, just incase you want to save the new image somewhere and then return the location
    String tmpImg = String.valueOf(System.currentTimeMillis()) + ".png";
        image.setImageBitmap(cs);
    OutputStream os = null;
    try {
      os = new FileOutputStream(Environment.getExternalStorageDirectory().getPath()+"/vote/" + tmpImg);
      cs.compress(Bitmap.CompressFormat.PNG, 100, os);
    } catch(IOException e) {
      Log.e("combineImages", "problem combining images", e);
    }

        return cs;
    }

    public static Bitmap overlay(Bitmap bmp2, Bitmap bmp1) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        //canvas.drawBitmap(bmp1, new Matrix(), null);
       // canvas.drawBitmap(bmp2, 0, 0, null);

        canvas.drawBitmap(bmp1, 0f, 0f, null);
        canvas.drawBitmap(bmp2, 0f, bmp1.getHeight(), null);

        return bmOverlay;
    }

    public void takePicture(){
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, 1337);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap url;
        if(resultCode == RESULT_OK){
            if(requestCode == 1337){
                image.setImageDrawable(null);
                image.destroyDrawingCache();
                url = (Bitmap)  data.getExtras().get("data");
              //  Bitmap bmp2 = BitmapFactory.decodeResource(getResources(),R.drawable.bg1);
               // Bitmap finalImage =  overlay(url,bmp2);
               // storeImage(finalImage);
                image.setImageBitmap(url);

            }
        }

    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.e("test",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 20, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e("test", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.e("test", "Error accessing file: " + e.getMessage());
        }
    }

    /** Create a File for saving an image or video */
    private  File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/electionapp");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        Log.e("test","File Path : "+mediaFile.getAbsolutePath());
        Toast.makeText(this, "File path : "+mediaFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
        return mediaFile;
    }
}
