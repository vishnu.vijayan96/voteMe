package in.projects.election.voteme.Utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.io.File;
import java.net.InetAddress;

import in.projects.election.voteme.R;

/**
 * Created by tltvmd15 on 24/3/16.
 */
public class Utils {

    /**
     * Show progress dialog.
     *
     * @param context the context
     * @param msg the msg
     * @return the progress dialog
     *
     */

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name

            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            return false;
        }

    }
    public static ProgressDialog showProgressDialog(final Context context,
                                                    final String msg) {
        ProgressDialog dialog = new ProgressDialog(context);

        try {
            dialog.setMessage(msg);
            dialog.setCancelable(false);
        } catch (final Exception ex) {
            ex.printStackTrace();
        }

        dialog.show();

        return dialog;
    }

    /**
     * Show progress.
     *
     * @param context the context
     * @param message the message
     */
    public static void showProgress(final Context context, final String message) {
        if(null != context)
            Progress.showProgress(context, message);
    }

    /**
     * Hide progress.
     */
    public static void hideProgress() {
        Progress.hideProgress();
    }

    public static void DeleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                DeleteRecursive(child);

        fileOrDirectory.delete();

    }

    public static void showInformationAlert(Context context, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.mipmap.ic_launcher).
                setTitle(R.string.app_name).
                setMessage(message).
                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();;

                    }
                }).
                show();
    }
}
