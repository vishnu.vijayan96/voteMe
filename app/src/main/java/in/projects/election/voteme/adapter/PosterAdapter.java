package in.projects.election.voteme.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import in.projects.election.voteme.R;
import in.projects.election.voteme.model.Poster;

public class PosterAdapter extends BaseAdapter{
	Activity adapterContext;
	ArrayList<Poster> posterList = new ArrayList<Poster>();
	ViewHolder viewholder;
	LayoutInflater mInflator;
	public PosterAdapter (Activity context, ArrayList<Poster> data){
		posterList = data;
		adapterContext = context;
		mInflator = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return posterList.size();
	}

	@Override
	public Object getItem(int pos) {
		return posterList.get(pos);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		try{
			if(view == null){
				view = mInflator.inflate(R.layout.listitem_posters, null);
				viewholder = new ViewHolder();
				viewholder.imgDownload = (ImageView) view.findViewById(R.id.img_item_download);
				viewholder.txtTitle = (TextView) view.findViewById(R.id.txt_item_title);

				viewholder.imgDownload.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						//call download function here.
						Toast.makeText(adapterContext, "Poster downloadig to sdcard", Toast.LENGTH_LONG).show();
					}
				});
				view.setTag(viewholder);
			}else{
				viewholder = (ViewHolder) view.getTag();
			}
			
			viewholder.txtTitle.setText(posterList.get(position).getName());
			adapterContext.startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse(posterList.get(position).getPoster_image())));
		}catch (Exception e){
			e.printStackTrace();
		}
		return view;
	}

	public class ViewHolder{
		ImageView imgDownload;
		TextView txtTitle;
	}
}






	