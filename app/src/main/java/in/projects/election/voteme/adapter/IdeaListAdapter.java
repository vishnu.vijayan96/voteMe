package in.projects.election.voteme.adapter;


import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import in.projects.election.voteme.R;
import in.projects.election.voteme.model.IdeaModel;

public class IdeaListAdapter extends BaseAdapter{
	Activity adapterContext;
	ArrayList<IdeaModel> ideaList = new ArrayList<IdeaModel>();
	ViewHolder viewholder;
	LayoutInflater mInflator;
	public IdeaListAdapter (Activity context, ArrayList<IdeaModel> data){
		ideaList = data;
		adapterContext = context;
		mInflator = LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return ideaList.size();
	}

	@Override
	public Object getItem(int pos) {
		return ideaList.get(pos);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		try{
			if(view == null){
				view = mInflator.inflate(R.layout.listitem_ideas, null);
				viewholder = new ViewHolder();
				viewholder.txtTitle = (TextView) view.findViewById(R.id.txt_idea_title);
				viewholder.txtDescription = (TextView) view.findViewById(R.id.txt_idea_description);
				view.setTag(viewholder);
			}else{
				viewholder = (ViewHolder) view.getTag();
			}
			
			viewholder.txtTitle.setText(ideaList.get(position).getTitle());
			viewholder.txtDescription.setText(ideaList.get(position).getDescription());
		}catch (Exception e){
			e.printStackTrace();
		}
		return view;
	}

	public class ViewHolder{
		TextView txtTitle;
		TextView txtDescription;
	}
}






	