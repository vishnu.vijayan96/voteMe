
package in.projects.election.voteme.db;

import java.util.ArrayList;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import in.projects.election.voteme.constants.Finals;
import in.projects.election.voteme.model.CampaignModel;
import in.projects.election.voteme.model.IdeaModel;
import in.projects.election.voteme.model.MediaModel;
import in.projects.election.voteme.model.Poster;
import in.projects.election.voteme.model.Profile;
import in.projects.election.voteme.model.SpeechModel;


public class ElectionDatabase extends SQLiteAssetHelper {


	private static final int DATABASE_VERSION = 1;

    private Context mContext = null;
	public ElectionDatabase(Context context) {
		super(context, DBConstants.DATABASE_NAME, null, null,DBConstants.DATABASE_VERSION);
        this.mContext = context;
      //  setForcedUpgrade();

	}


	public void createProfile(Profile profile){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(DBConstants.ProfileTables.colCandidateName, profile.getCandidate_name());
            cv.put(DBConstants.ProfileTables.colAbout, profile.getAbout());
            cv.put(DBConstants.ProfileTables.colBiography, profile.getBiography());
            cv.put(DBConstants.ProfileTables.colAgitations, profile.getAgitations());
            cv.put(DBConstants.ProfileTables.colProfilePath, profile.getCandidate_name());
            cv.put(DBConstants.ProfileTables.colAppShareUrl,profile.getAppShare_url());
            long row = db.insert(DBConstants.Tables.profileTable, DBConstants.ProfileTables.colProfileId, cv);
            if (row == -1) {
                Log.e(Finals.TAG, "Insert failed");

            } else
                Log.e(Finals.TAG, "Insert one row");
            db.close();
        }catch(Exception e){
            e.printStackTrace();
        }
	}
	
	public void insertMandalamData(ArrayList<IdeaModel> idea){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //ContentValues cv = null;
            for (int i = 0; i < idea.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(DBConstants.MandalamTable.colMandalamTopic, idea.get(i).getTitle());
                cv.put(DBConstants.MandalamTable.colMandalamDescpn, idea.get(i).getDescription());

                db.insert(DBConstants.Tables.mandalamTable, DBConstants.MandalamTable.colMandalamId, cv);
            }
            db.close();
        }catch(Exception e){
            e.printStackTrace();
        }
	}

    public void insertSpeechData(ArrayList<SpeechModel> idea){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //ContentValues cv = null;
            for (int i = 0; i < idea.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(DBConstants.SpeechTable.colSpeechTopic, idea.get(i).getTitle());
                cv.put(DBConstants.SpeechTable.colSpeechDescpn, idea.get(i).getDescription());

                db.insert(DBConstants.Tables.speechTable, DBConstants.SpeechTable.colSpeechId, cv);
            }
            db.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void insertMediaData(ArrayList<MediaModel> media){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //ContentValues cv = null;
            for (int i = 0; i < media.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(DBConstants.mediaTable.colMediaTitle, media.get(i).getMedia_title());
                cv.put(DBConstants.mediaTable.colMediaPath, media.get(i).getMedia_url());
                cv.put(DBConstants.mediaTable.colMediaType, media.get(i).getMedia_type());
                db.insert(DBConstants.Tables.mediaTable, DBConstants.mediaTable.colMediaId, cv);
            }
            db.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void insertPosterData(ArrayList<Poster> poster){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //ContentValues cv = null;
            for (int i = 0; i < poster.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(DBConstants.postersTable.colPosterTitle, poster.get(i).getName());
                cv.put(DBConstants.postersTable.colPosterPath, poster.get(i).getPoster_url());

                db.insert(DBConstants.Tables.postersTable, DBConstants.postersTable.colPosterId, cv);
            }
            db.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void insertCampaignData(ArrayList<CampaignModel> campaign){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //ContentValues cv = null;
            for (int i = 0; i < campaign.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(DBConstants.campaignTable.colCampaignTitle, campaign.get(i).getTitle());
                cv.put(DBConstants.campaignTable.colCampaignDescpn, campaign.get(i).getDescription());
                cv.put(DBConstants.campaignTable.colCampaignPlace, campaign.get(i).getPlace());
                cv.put(DBConstants.campaignTable.colCampaignDate, campaign.get(i).getDate());
                cv.put(DBConstants.campaignTable.colCampaignTime, campaign.get(i).getTime());
                db.insert(DBConstants.Tables.campaignTable, DBConstants.campaignTable.colCampaignId, cv);
            }
            db.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public Profile getProfileData() {
		Profile candidateProfile = null;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + DBConstants.Tables.profileTable, new String[] {});
		cur.moveToFirst();
		if(cur.getCount() > 0){
            candidateProfile = new Profile();
			candidateProfile.setAbout(cur.getString(cur.getColumnIndex(DBConstants.ProfileTables.colAbout)));
			candidateProfile.setCandidate_name(cur.getString(cur.getColumnIndex(DBConstants.ProfileTables.colCandidateName)));
			candidateProfile.setAgitations(cur.getString(cur.getColumnIndex(DBConstants.ProfileTables.colAgitations)));
			candidateProfile.setBiography(cur.getString(cur.getColumnIndex(DBConstants.ProfileTables.colBiography)));
            candidateProfile.setAppShare_url(cur.getString(cur.getColumnIndex(DBConstants.ProfileTables.colAppShareUrl)));
			//candidateProfile.setProfile_image(cur.getBlob(cur.getColumnIndex(DBConstants.ProfileTables.colProfilePath)));
		}
		db.close();
		return candidateProfile;
	  }

	public  ArrayList<IdeaModel> getMandalamData(){
        ArrayList<IdeaModel> ideaList = new ArrayList<IdeaModel>();
		IdeaModel ideas = null;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + DBConstants.Tables.mandalamTable, new String[] {});
		cur.moveToFirst();
		if(cur.getCount() > 0){
            for(cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                ideas = new IdeaModel(cur.getString(cur.getColumnIndex(DBConstants.MandalamTable.colMandalamTopic)),
                        cur.getString(cur.getColumnIndex(DBConstants.MandalamTable.colMandalamDescpn)));

				ideaList.add(ideas);
			}
			
		}
		return ideaList;
	}

    public  ArrayList<SpeechModel> getSpeechData(){
        ArrayList<SpeechModel> speechList = new ArrayList<SpeechModel>();
        SpeechModel speech = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + DBConstants.Tables.speechTable, new String[] {});
        cur.moveToFirst();
        if(cur.getCount() > 0){
            for(cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                speech = new SpeechModel(cur.getString(cur.getColumnIndex(DBConstants.SpeechTable.colSpeechTopic)),
                        cur.getString(cur.getColumnIndex(DBConstants.SpeechTable.colSpeechDescpn)));

                speechList.add(speech);
            }

        }
        return speechList;
    }

    public  ArrayList<MediaModel> getMediaData(){
        ArrayList<MediaModel> mediaList = new ArrayList<MediaModel>();
        MediaModel media = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + DBConstants.Tables.mediaTable, new String[] {});
        cur.moveToFirst();
        if(cur.getCount() > 0){
            for(cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                media = new MediaModel(cur.getString(cur.getColumnIndex(DBConstants.mediaTable.colMediaTitle)),
                        cur.getString(cur.getColumnIndex(DBConstants.mediaTable.colMediaPath)),
                        cur.getString(cur.getColumnIndex(DBConstants.mediaTable.colMediaType)));

                mediaList.add(media);
            }

        }
        return mediaList;
    }

    public  ArrayList<Poster> getPostersData(){
        ArrayList<Poster> posterList = new ArrayList<Poster>();
        Poster poster = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + DBConstants.Tables.postersTable, new String[] {});
        cur.moveToFirst();
        if(cur.getCount() > 0){
            for(cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                poster = new Poster(cur.getString(cur.getColumnIndex(DBConstants.postersTable.colPosterTitle)),
                        cur.getString(cur.getColumnIndex(DBConstants.postersTable.colPosterPath)));

                posterList.add(poster);
            }

        }
        return posterList;
    }

    public  ArrayList<CampaignModel> getCampaignData(){
        ArrayList<CampaignModel> campaignList = new ArrayList<CampaignModel>();
        CampaignModel campaign = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM " + DBConstants.Tables.campaignTable, new String[] {});
        cur.moveToFirst();
        if(cur.getCount() > 0){
            for(cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                campaign = new CampaignModel();
                campaign.setTitle(cur.getString(cur.getColumnIndex(DBConstants.campaignTable.colCampaignTitle)));
                campaign.setDescription(cur.getString(cur.getColumnIndex(DBConstants.campaignTable.colCampaignDescpn)));
                campaign.setPlace(cur.getString(cur.getColumnIndex(DBConstants.campaignTable.colCampaignPlace)));
                campaign.setTime(cur.getString(cur.getColumnIndex(DBConstants.campaignTable.colCampaignTime)));
                campaign.setDate(cur.getString(cur.getColumnIndex(DBConstants.campaignTable.colCampaignDate)));
                campaignList.add(campaign);
            }

        }
        return campaignList;
    }


    public void clearTable(String tablename){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(tablename, null, null);
            db.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
